﻿using System;
using UnityEngine;

namespace Manager
{
    public class SoundManager : MonoBehaviour
    {
        //[SerializeField] private AudioClip[] audioClips;
        [SerializeField] private SoundCilp[] soundCilps;
        [SerializeField] private AudioSource audioSource;
        
        public enum Sound
        {
            BGM,
            PlayerFire,
            EnemyFire,
            BGM2,
        }
        
        [Serializable]
        public struct SoundCilp
        {
            public Sound sound;
            public AudioClip audioClip;
            [Range(0,1)]public float soundVolume;
        }

        public void Awake()
        {
            Debug.Assert(soundCilps != null && soundCilps.Length != 0,"sound clips need to be setup");
            Debug.Assert(audioSource != null, "audioSource cannot be null");
        }

        public void Play(AudioSource audioSource,Sound sound)
        {
            var soundCilp = GetSoundClip(sound);
            audioSource.clip = soundCilp.audioClip;
            audioSource.volume = soundCilp.soundVolume;
            audioSource.Play();
        }

        public void PlayBGM()
        {
            audioSource.loop = true;
            Play(audioSource,Sound.BGM);
        }
        public void PlayBGM2()
        {
            audioSource.loop = true;
            Play(audioSource,Sound.BGM2);
        }

        private SoundCilp GetSoundClip(Sound sound)
        {
            foreach (var soundCilp in soundCilps)
            {
                if (soundCilp.sound == sound)
                {
                    return soundCilp;
                }
            }

            return default(SoundCilp);
        }
        
    }

    
}
